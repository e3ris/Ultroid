# additional functions that must be seperate from commons..

__all__ = (
    "aiohttp",
    "cpu_bound",
    "run_async",
    "async_searcher",
    "FixedSizeDict",
)

import asyncio
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from collections import UserDict
from functools import partial, wraps
import time

from pyUltroid.exceptions import DependencyMissingError
from ._loop import loop, run_async_task

aiohttp = None  # prefer using requests over this.

try:
    import requests
except ImportError:
    requests = None


"""
# this shit doesn't works and causes weird errors

try:
    import aiodns
except ImportError:
    aiodns = None

# dns for async_searcher
_kwargs = {"ttl_dns_cache": 120, "loop": loop}
if aiohttp and aiodns:
    resolver = aiohttp.resolver.AsyncResolver(
        nameservers=[
            "1.1.1.1",
            "1.0.0.1",
            "2606:4700:4700::1111",
            "2606:4700:4700::1001",
            "8.8.8.8",
        ]
    )
    _kwargs["resolver"] = resolver

connector = aiohttp.TCPConnector(**_kwargs)
"""


_workers = __import__("multiprocessing").cpu_count()


# source: fns/helper.py
# preferred for I/O bound task.
def run_async(function):
    @wraps(function)
    async def wrapper(*args, **kwargs):
        return await loop.run_in_executor(
            ThreadPoolExecutor(max_workers=_workers * 2),
            partial(function, *args, **kwargs),
        )

    return wrapper


# preferred for cpu bound tasks.
def cpu_bound(function):
    @wraps(function)
    async def wrapper(*args, **kwargs):
        with ProcessPoolExecutor(max_workers=_workers) as pool:
            output = await loop.run_in_executor(
                pool,
                partial(function, *args, **kwargs),
            )
        return output

    return wrapper


class FixedSizeDict(UserDict):
    """dict with maxsize"""

    __slots__ = ("maxsize", "data")

    def __init__(self, *args, **kwargs):
        self.maxsize = kwargs.pop("maxsize", 32)
        super().__init__(*args, **kwargs)

    def __setitem__(self, key, value):
        self.data[key] = value
        if len(self.data) > self.maxsize:
            self.data.pop(next(iter(self.data)), None)


# source: fns/helper.py
# Async Searcher -> @buddhhu

if aiohttp:

    async def _downloader(response, filename, progress_callback):
        total_size = int(response.headers.get("content-length", 0)) or None
        downloaded_size = 0
        start_time = time.time()
        with open(filename, "wb+") as f:
            async for chunk in response.content.iter_chunked(256 * 1024):
                if chunk:
                    f.write(chunk)
                    downloaded_size += len(chunk)
                    if progress_callback and total_size:
                        await progress_callback(downloaded_size, total_size)

        return filename, time.time() - start_time

    async def async_searcher(
        url: str,
        post: bool = False,
        method: str = "GET",
        headers: dict = None,
        evaluate: callable = None,
        download: bool = False,
        object: bool = False,
        re_json: bool = False,
        re_content: bool = False,
        timeout: int | bool = 60,
        **kwargs,
    ):
        method = "POST" if post else method.upper()

        if timeout:
            timeout = aiohttp.ClientTimeout(total=int(timeout))
        async with aiohttp.ClientSession(headers=headers) as client:
            data = await client.request(method, url, timeout=timeout, **kwargs)
            if evaluate:
                from telethon.helpers import _maybe_await

                return await _maybe_await(evaluate(data))
            elif download:
                return await _downloader(
                    data, kwargs["filename"], kwargs.get("progress_callback")
                )
            elif re_json:
                return await data.json()
            elif re_content:
                return await data.read()
            elif object:
                return data
            else:
                return await data.text()

elif requests:

    def _downloader(response, filename, progress_callback):
        total_size = int(response.headers.get("content-length", 0)) or None
        downloaded_size = 0
        start_time = time.time()
        with open(filename, "wb+") as f:
            for chunk in response.iter_content(256 * 1024):
                if chunk:
                    f.write(chunk)
                    downloaded_size += len(chunk)
                    if progress_callback and total_size:
                        run_async_task(progress_callback, downloaded_size, total_size)

        return filename, time.time() - start_time

    @run_async
    def async_searcher(
        url: str,
        post: bool = False,
        method: str = "GET",
        headers: dict = None,
        evaluate: callable = None,
        download: bool = False,
        object: bool = False,
        re_json: bool = False,
        re_content: bool = False,
        timeout: int | bool = 60,
        **kwargs,
    ):
        method = "POST" if post else method.upper()
        if "ssl" in kwargs:
            kwargs["verify"] = kwargs.pop("ssl", None)

        filename = kwargs.pop("filename", "downloaded_file")
        progress_callback = kwargs.pop("progress_callback", None)

        data = requests.request(
            method,
            url,
            headers=headers,
            timeout=timeout,
            **kwargs,
        )
        if re_json:
            return data.json()
        elif download:
            return _downloader(data, filename, progress_callback)
        elif re_content:
            return data.content
        elif evaluate:
            return evaluate(data)
        elif object:
            return data
        else:
            return data.text

        return data

else:
    raise DependencyMissingError("Install 'requests' to use this.")
